package hr.fer.zemris.java.custom.collections;

/**
 * Exception used within the ObjectStack class when the stack is empty and user
 * is trying to pop it.
 * 
 * @author Mihovil Vinković
 */
public class EmptyStackException extends RuntimeException {

	private static final long serialVersionUID = 1L;

}
