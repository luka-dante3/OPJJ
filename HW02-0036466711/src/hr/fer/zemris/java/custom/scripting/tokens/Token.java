package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Basic token used. for inheritance
 * @author Mihovil Vinković
 *
 */
public class Token {
	
	/**
	 * @return Text representation of the token.
	 */
	public String asText() {
		return "";
	}
}
