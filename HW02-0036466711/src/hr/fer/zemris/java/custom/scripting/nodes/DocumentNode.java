package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * A node at the top of the node hierarchy representing a whole document,
 * 
 * @author Mihovil Vinković
 */
public class DocumentNode extends Node {

}
