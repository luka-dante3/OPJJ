package hr.fer.zemris.java.custom.scripting.parser;

/**
 * Used for signaling that a parsing error has happened.
 * Exception for use with custom parser.
 * 
 * @author Erik Banek
 */
public class SmartScriptParserException extends RuntimeException {
	private static final long serialVersionUID = -7146405271722626823L;
	
	/**
	 * Constructs a parser exception.
	 */
	public SmartScriptParserException() {
		
	}
}
