package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Razred <code>Token</code> vraća prazan string.
 * @author Rade Bebek
 * @since 2015-03-25  
 */
public class Token {

	/**
	 * Metoda <code>asText</code> vraća string.
	 * @return string
	 * @author Rade Bebek <rade.bebek@icloud.com>
	 * @since 2015-03-25 
	 */
	public String asText(){
		return "";
	}


}
