package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Razred <code>DocumentNode</code> je korjen dokumenta koji se parsira.
 * @author Rade Bebek
 * @since 2015-03-25  
 */
public class DocumentNode extends Node{


	/**
	 * Metoda <code>getTekst</code> vraća prazan string.
	 * @return prazan string.
	 * @author Rade Bebek <rade.bebek@icloud.com>
	 * @since 2015-03-25 
	 */
	public String getTekst(){
		return "";
	}
}